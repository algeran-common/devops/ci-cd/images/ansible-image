ANSIBLE IMAGE
-------------

Ansible image with linter. Based on alpine

To use that image in projects with ansible playbooks
 you need to do following steps:

 1. Generate couple of SSH keys
     ```
     $ ssh-keygen -t rsa -b 2048
     ```
    or use existing one:
    ```
    cicd-key
    cicd-key.pub
    ```
2. add private key to project CI variable with type file
3. add public key to every one host that will be used in playbook.
Public key should be added to ~/.ssh/authorized_keys in home directory
of user that will be used in playbook as remote user
4. Example of .gitlab-ci.yml:
```
stages:
  - lint_syntax
  - ansible_playbook

# playbook and roles linter
check_ansible_syntax:
  stage: lint_syntax
  image: algeran/ansible:2.9.6-alpine
  script:
    - ansible-lint some-playbook.yml

# playbook run
# Required and available things:
# 1) ANSIBLE_HOST_KEY_CHECKING: "False" is used to ignore host key checking on first SSH connection to host
# 2) --private-key $ANSIBLE_PRIVATE_KEY is used to get private key from project CI variables to SSH connection
# 3) --vault-password-file $VAULT_PASSWORD_FILE - is used to decrypt vault secrets with file secret in project CI varuables
run_ansible:
  stage: ansible_playbook
  image: algeran/ansible:2.9.6-alpine
  variables:
    ANSIBLE_HOST_KEY_CHECKING: "False"
    INTERPRETER_PYTHON: "auto_silent"
  script:
    - ansible-playbook
      -i hosts
      --private-key $ANSIBLE_PRIVATE_KEY
      --vault-password-file $VAULT_PASSWORD_FILE
      some-playbook.yml
```
