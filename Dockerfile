FROM        python:3-alpine

ARG         ANSIBLE_VERSION
ARG         LINUX_HEADERS_VERSION
ARG         GCC_VERSION
ARG         MUSL_VERSION
ARG         LIBFFI_VERSION
ARG         OPENSSL_VERSION
ARG         MAKE_VERSION
ARG         PARAMIKO_VERSION
ARG         JMESPATH_VERSION
ARG         ANSIBLE_LINT_VERSION

RUN         apk update && \
                # install requirements for ansible
                apk add linux-headers=${LINUX_HEADERS_VERSION} \
                        gcc=${GCC_VERSION} \
                        musl-dev=${MUSL_VERSION} \
                        libffi-dev=${LIBFFI_VERSION} \
                        openssl-dev=${OPENSSL_VERSION} \
                        make=${MAKE_VERSION} && \
                # install ansible
                pip install ansible==${ANSIBLE_VERSION} && \
                # install paramiko for ssh calls
                pip install paramiko==${PARAMIKO_VERSION} && \
                # install jmespath for JSON jpath operations in tasks
                pip install jmespath==${JMESPATH_VERSION} && \
                # install ansible-lint
                pip install ansible-lint==${ANSIBLE_LINT_VERSION} && \
                # create ansible workdir
                mkdir ansible

WORKDIR     /ansible
